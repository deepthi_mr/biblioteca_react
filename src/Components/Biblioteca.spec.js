import React from 'react';
import {mount,shallow} from 'enzyme';
import { Route } from 'react-router';
import Biblioteca from './Biblioteca';
import Welcome from './Welcome';
import DisplayBooks from './DisplayBooks';

describe('Biblioteca',()=>{

    it('should routes / to Welcome ',()=>{
        const wrapper = shallow(<Biblioteca/>)
        const homeRoute = wrapper.find(Route).at(0);
        expect(homeRoute.props().path).toEqual('/');
        expect(homeRoute.props().component).toEqual(Welcome);
    });

    it('should routes /books to DisplayBooks ',()=>{
        const wrapper = shallow(<Biblioteca/>)
        const booksRoute = wrapper.find(Route).at(1);
        expect(booksRoute.props().path).toEqual('/books');
        expect(booksRoute.props().component).toEqual(DisplayBooks);
    });
});
