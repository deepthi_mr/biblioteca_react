import React,{Component} from 'react';
import DisplayBooks from './DisplayBooks';
class ReturnBook extends Component{
    constructor(props){
        super(props);
        this.state={
            checkedOutBooks:[],
        }
    }


    componentDidMount(){
        this.setState({
            checkedOutBooks: this.props.books.filter((book)=>{
                return book.checkOutStatus === true;
                    
            }),
        });
        this.props.checkoutbooks.push(this.state.checkedOutBooks);
    }

    
    render(){
        return(
            <div>
                <DisplayBooks books={this.state.checkedOutBooks} 
                              checkout={this.props.checkout}
                              returnBook={this.props.returnBook}/>
            </div>
        );
    }
}

export default ReturnBook;