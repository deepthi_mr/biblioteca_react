import React from 'react';
import {MemoryRouter,Link} from 'react-router-dom';
import {shallow,render,mount} from 'enzyme';
import Welcome from './Welcome';

describe('Welcome',()=>{
    it('should render welcome component without crashing',()=>{
        shallow(<Welcome/>);
    });

    it('should contain the text Library Management System Your can see the books Here',()=>{
        const wrapper = shallow(<Welcome/>);
        expect(wrapper.text()).toBe('Library Management SystemYou can See the books Here');
    });

    it('should contain the Link books',()=>{
        const wrapper = mount(
            <MemoryRouter>
                <Welcome/>
            </MemoryRouter>
        );
        expect(wrapper.find(Link).prop('to')).toBe('/books');
    });
});