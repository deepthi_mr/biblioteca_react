import React,{Component} from 'react';
import {withRouter} from 'react-router-dom';
import './DisplayBooks.css'
class DispalyBooks extends Component{
   
    handleCheckout = (bookId) =>{
        this.props.checkout(bookId);
    }

    handleReturn = (bookId) =>{
        this.props.returnBook(bookId);
    }

   render(){
        const text = (this.props.books.length !==0)?'Books':'No Books Available';
        return(
            <div className='container'>
                <h1>{text}</h1>
                {this.props.books.length !==0 && <div className="container">
                <table className="table">
                        <thead>
                            <tr>
                                <th>Book Name</th>
                                <th>Author</th>
                                <th>Published Year</th>
                                <th>Book Type</th>
                                <th>Status</th>
                                <th>Checkout/Return</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.props.books.map((book)=>
                                <tr key = {book.id}>
                                    <td>{book.name}</td>
                                    <td>{book.author}</td>
                                    <td>{book.publishedYear}</td>
                                    <td>{book.type}</td>
                                    <td>
                                        {book.checkOutStatus && <span className='checkout'>CheckedOut</span>}
                                        {book.returnStatus && <span className='available'>Available</span>}
                                    </td>
                                    <td>
                                        {
                                            !book.checkOutStatus && 
                                            <button className = "btn btn-primary"
                                                    onClick={()=> this.handleCheckout(book.id)}>Checkout
                                            </button>
                                        }
                                        {
                                            !book.returnStatus && 
                                            <button className="btn btn-warning"
                                                    onClick={()=>{this.handleReturn(book.id)}}>Return
                                            </button>
                                        }
                                    </td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>}
            </div> 
        );
    }
}

export default withRouter(DispalyBooks);