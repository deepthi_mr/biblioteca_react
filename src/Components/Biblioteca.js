import React,{Component} from 'react';
import {BrowserRouter as Router, Route,Switch} from 'react-router-dom';
import Welcome from './Welcome';
import './Biblioteca.css'
import DispalyBooks from './DisplayBooks';
import AddBook from './AddBook';
import ReturnBook from './ReturnBook';
import Header from './Header';

class Biblioteca extends Component{
    constructor(props){
        super(props);
        this.state = {
            books :[
                {
                    id:0,
                    name:'The power of subconscious mind',
                    author:'Joseph Murphy',
                    publishedYear:'1963',
                    type:'Self-help book',
                    checkOutStatus:false,
                    returnStatus:true,
                },{
                    id:1,
                    name:'The monk who sold his ferrari',
                    author:'Robin Sharma',
                    publishedYear:'1996',
                    type:'Fable',
                    checkOutStatus:false,
                    returnStatus:true,
                }
            ],
            availableBooks:[],
            checkedOutBooks:[],
            
        };
    }

    componentDidMount(){
        this.updateAvailableBooks();
    }

    updateAvailableBooks = ()=>{
        this.setState({
            availableBooks: this.state.books.filter((book)=>{
                return book.checkOutStatus === false;
            }),
        });
    }
    
    checkoutBook = (bookId)=>{
        console.log('checking out book');
        console.log(this.state.books);
        let books = this.state.books;
        books[bookId].checkOutStatus = true;
        books[bookId].returnStatus = false;
        this.setState({
            books: books,
        });
        this.updateAvailableBooks();
    }

    returnBook = (bookId) => {
        console.log('returing book');
        let books = this.state.books;
        books[bookId].checkOutStatus = false;
        books[bookId].returnStatus = true;
        this.setState({
            books: books,
        });
        this.updateAvailableBooks();
    }

    render(){
        return(
            <div>
                <Router>
                    <>
                        <Header/>
                        <Switch>
                            <Route path="/" exact component={
                                ()=> <Welcome books={this.state.availableBooks}/>
                                }/>
                            <Route path="/books" exact component = {
                                () => <DispalyBooks books={this.state.availableBooks} 
                                                  checkout={this.checkoutBook}
                                                  returnBook={this.returnBook}/>
                            }/>
                            <Route path="/addBook" exact component = {
                                ()=><AddBook books={this.state.books}
                                            availableBooks = {this.state.availableBooks}
                                             />
                            }/>

                            <Route path="/returnBook" exact component = {
                                ()=><ReturnBook books={this.state.books}
                                        checkoutbooks={this.state.checkedOutBooks}
                                        checkout={this.checkoutBook}
                                        returnBook={this.returnBook}
                                        />
                            }/>

                        </Switch>
                    </>
                </Router>
            </div>
        );
    }
}

export default Biblioteca;