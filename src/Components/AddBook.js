import React,{Component} from 'react';
import { withRouter } from 'react-router';
import './AddBook.css'
class AddBook extends Component{
    constructor(props){
        super(props);
        this.state = {
            id : this.props.books.length,
            name:'',
            author:'',
            publishedYear:'',
            type:'',
            checkoutStatus:false,
            returnStatus:true,
        }
    }

    handleBookName = (event)=>{
        this.setState({
            name : event.target.value,
        });
    }

    handleAuthorName = (event)=>{
        this.setState({
            author : event.target.value,
        });
    }

    handlePublishedYear = (event) => {
        this.setState({
            publishedYear : event.target.value,
        });
    }

    handleBookType = (event) => {
        this.setState({
            type : event.target.value,
        });
    }

    handleSubmit = ()=>{
        this.props.books.push(this.state);
        this.props.availableBooks.push(this.state);
        this.props.history.push('/books');
    }

    render(){
        const {name,author,publishedYear,type} = this.state;
        return(
            <div className='addform container'>
            <form>
                    <div className='form-group'>
                        <label>Book Name</label>
                        <input  className='form-control' 
                                type='text' value={name} 
                                onChange={this.handleBookName} 
                        />
                    </div>
                    <div className='form-group'>
                        <label>Author</label>
                        <input  className='form-control' 
                                type='text' value={author} 
                                onChange={this.handleAuthorName}
                        />
                    </div>
                    <div className='form-group'>
                        <label>Published Year</label>
                        <input  className='form-control' 
                                type='text' value={publishedYear} 
                                onChange={this.handlePublishedYear} 
                        />
                    </div>
                    <div className='form-group'>
                        <label>Type</label>
                        <input  className='form-control' 
                                type='text' value={type} 
                                onChange={this.handleBookType}
                        />
                    </div>
                    <button className='btn btn-success' 
                            onClick={this.handleSubmit}>Save
                    </button>
                </form>
                
            </div>
            
        );
    }
}
export default withRouter(AddBook);