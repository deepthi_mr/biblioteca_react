import React,{Component} from 'react'
import {Link,withRouter} from 'react-router-dom'
import './Welcome.css'
class Welcome extends Component{
    
    handleCheckout = ()=>{
        this.props.history.push('/books');
    }

    handleReturn = () =>{
        this.props.history.push('/returnBook');
    }

    handleAdd = () => {
        this.props.history.push('/addBook');
    }

    render(){
        return(
            <div className="container">
                <h1>Library Management System</h1>
                You can See the books 
                <Link to="/books"> Here</Link>
                <div className="row justify-content-sm-center">
                    
                </div>
                <div className='row justify-content-sm-center'>
                    <button className='btn btn-success col-sm-2' 
                            onClick={this.handleAdd}>AddBook
                    </button> 
                    <button className="col-sm-2 offset-1 btn btn-primary"
                            onClick={this.handleCheckout}>CheckoutBook
                    </button>
                    <button className= 'col-sm-2 offset-1 btn btn-warning'
                            onClick={this.handleReturn}>ReturnBook
                    </button>
                </div>
                
            </div>
        );
    }
}
export default withRouter(Welcome);