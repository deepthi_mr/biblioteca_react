import React from 'react';
import {Link} from 'react-router-dom';
function Header(){
    return(
        <header>
            <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                <div className="navbar-header">
                    <Link className="navbar-brand" to="/">DDLMS</Link>
                </div>
                <ul className="navbar-nav">
                    <li><Link className="nav-link" to="/books">Books</Link></li>
                </ul>
                
            </nav>

        </header>
    );
}
export default Header;