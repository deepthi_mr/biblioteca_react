import React from 'react';
import './App.css';
import Biblioteca from './Components/Biblioteca';

function App() {
  return (
    <div className="App">
          <Biblioteca/>
    </div>
  );
}

export default App;
